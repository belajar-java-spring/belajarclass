package id.guls.belajarclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarclassApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarclassApplication.class, args);
	}

}
