package id.guls.belajarclass.controller;

import id.guls.belajarclass.entity.AManyMemberClass;
import id.guls.belajarclass.entity.AManyMemberWithBuilderClass;
import id.guls.belajarclass.entity.City;
import id.guls.belajarclass.entity.VanillaClass;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/basic")
public class BasicController {

    @RequestMapping(method = RequestMethod.GET, value = "/vanilla/default")
    public VanillaClass vanillaDefaultConstructor() {
        VanillaClass vanillaClass = new VanillaClass();

        vanillaClass.setId(1);
        vanillaClass.setName("default constructor");

        return vanillaClass;
    }

    @GetMapping("/vanilla/defined")
    public VanillaClass vanillaDefinedConstructor() {
        VanillaClass vanillaClass = new VanillaClass(1, "defined constructor");

        return vanillaClass;
    }

    @GetMapping("/members1")
    public AManyMemberClass manyMemberClass1() {
        AManyMemberClass aManyMemberClass = new AManyMemberClass(1L, "Unggul", "Jl. Madukara", "Magelang", "51115", "ID", 120d);

        return aManyMemberClass;
    }

    @GetMapping("/members2")
    public AManyMemberWithBuilderClass manyMemberClass2() {
        AManyMemberWithBuilderClass aManyMemberClass =
                AManyMemberWithBuilderClass
                        .builder()
                        .id(1L)
                        .name("Unggul")
                        .build();

        return aManyMemberClass;
    }

    @GetMapping("/city")
    public City getCities() {
        City city = new City();

        city.setId(1L);
        city.setName("Amarta");

        return city;
    }
}
