package id.guls.belajarclass.entity;

import lombok.Data;

@Data
public class AManyMemberClass {

    private Long id;

    private String name;

    private String address;

    private String city;

    private String zipCode;

    private String country;

    private Double salary = 100d;

    public AManyMemberClass(Long id, String name, String address, String city, String zipCode, String country, Double salary) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.country = country;
        this.salary = salary;
    }
}
