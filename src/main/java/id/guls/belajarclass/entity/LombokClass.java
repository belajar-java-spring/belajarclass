package id.guls.belajarclass.entity;

import lombok.Data;

@Data
public class LombokClass {

    private Integer id;

    private Integer name;

}
