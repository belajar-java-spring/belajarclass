package id.guls.belajarclass.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AManyMemberWithBuilderClass {

    private Long id;

    private String name;

    private String address;

    private String city;

    private String zipCode;

    private String country;

    private Double salary = 100d;

}
