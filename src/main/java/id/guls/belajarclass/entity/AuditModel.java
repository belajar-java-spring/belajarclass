package id.guls.belajarclass.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AuditModel {

    private String createdBy = "puntadewa";

    private LocalDateTime createdDate = LocalDateTime.now();

    private String updatedBy = "yudhistira";

    private LocalDateTime updatedDate = LocalDateTime.now();
}
