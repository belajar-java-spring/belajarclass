package id.guls.belajarclass.entity;

import lombok.Data;

@Data
public class City extends AuditModel {

    private Long id;

    private String name;

}
