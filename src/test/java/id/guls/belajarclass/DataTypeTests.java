package id.guls.belajarclass;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class DataTypeTests {

    @Test
    public void primitiveIntegerTest() {
        int primitiveInt = 10;
        log.info("primitiveInt: {}", primitiveInt );

        Integer wrappedInt = 10;
        String stringFromInt = wrappedInt.toString();
        log.info("stringFromInt: {}", stringFromInt);

        double doubleFromInt = wrappedInt.doubleValue();
        log.info("doubleFromInt: {}", doubleFromInt);

        float floatFromInt = wrappedInt.floatValue();
        log.info("floatFromInt: {}", floatFromInt);
    }

}
